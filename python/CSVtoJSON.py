import os
import csv
import json

currentPath = os.getcwd()


def csv_to_json():
    data_dict = {}

    with open(currentPath + '/python/roteiro.csv', encoding='utf-8') as csv_file_handler:
        csv_reader = csv.DictReader(csv_file_handler)

        for row in csv_reader:
            if row['page'] not in data_dict:
                data_dict[row['page']] = []
            data_dict[row['page']].append(get_keys(row))

    print_to_json(data_dict)


def get_keys(row):
    current_row = {}
    for attr in row:
        if not attr == 'page':
            current_row[attr] = row[attr]
    return current_row


def print_to_json(data_dict):
    with open(currentPath + '/src/data/data.json', 'w', encoding='utf-8') as json_file_handler:
        json_file_handler.write(json.dumps(data_dict, indent=4))
    print('done!')


csv_to_json()
