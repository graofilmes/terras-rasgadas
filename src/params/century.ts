/** @type {import('@sveltejs/kit').ParamMatcher} */
export function match(param) {
  return /(XVII|XVIII|XIX|XX)/.test(param)
}