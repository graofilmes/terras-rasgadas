export const millisecondsToTimecode = (currentTime: number): string => {
  const minutes = Math.floor(currentTime % 3600 / 60)
  const seconds = Math.floor(currentTime % 3600 % 60)
  return `${minutes < 10 ? '0' + minutes : minutes}:${seconds < 10 ? '0' + seconds : seconds}`
}

export const timecodeToMilliseconds = (timecode: string): number => {
  const time = timecode.split(':')
  const minutes = (time.length > 2 ? parseInt(time[1]) : parseInt(time[0])) * 60
  const seconds = time.length > 2 ? parseInt(time[2]) : parseInt(time[1])
  return  (minutes+seconds)*1000
}