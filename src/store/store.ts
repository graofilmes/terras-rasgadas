import { writable } from 'svelte/store'

function captions() {
	const { subscribe, set } = writable(false);

	return {
		subscribe,
		enable: () => set(true),
		disable: () => set(false),
    set: (value: boolean) => set(value)
	}
}

function signLanguage() {
	const { subscribe, set } = writable(false);

	return {
		subscribe,
		enable: () => set(true),
		disable: () => set(false),
    set: (value: boolean) => set(value)
	}
}

export const cc = captions()
export const libras = signLanguage()
