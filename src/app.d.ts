// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
declare namespace App {
  // interface Error {}
  // interface Locals {}
  // interface PageData {}
  // interface Platform {}
}

interface currentTime {
  duration: number
  percent: number
  seconds: number
}

type Century = "XVII" | "XVIII" | "XIX" | "XX"

interface Data {[key: Century]: Icon}[]

interface Icon {
  type: "video" | "text" | "display" | "none"
  title: string
  left: number
  top: number
  width: number
  alt: string
  src: string
  img: string
  x: number
  y: number
  size: number
}