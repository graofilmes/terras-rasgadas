/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        'theme': {
          'bg': '#E8CAB2',
          'selected': '#DABCA4',
          'highlight': '#E5BA93',
          'map': '#E9C59F'
        },
        'icon': {
          'lighter': '#E8DED2',
          'light': '#DFB669',
          'base': '#E0A118',
          'dark': '#412D0B'
        }
      },
      screens: {
        'touch': {'raw' : '(pointer: coarse)'}
      }
    }
  },
  plugins: []
};